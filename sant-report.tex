\documentclass{article}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% \usepackage{amsthm}
\usepackage{graphicx}
\usepackage[draft]{fixme}
\usepackage[pdfborder={0 0 0}]{hyperref}
\usepackage[english]{babel}
\usepackage{rotating}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{enumerate}
%\usepackage[backend=biber,style=numeric-comp,doi=false,isbn=false,url=false,firstinits=true]{biblatex}
\usepackage{textcomp}
\usepackage{booktabs}
\usepackage{datetime}
\usepackage{wrapfig}
\usepackage{paralist}
\usepackage{csquotes}
\usepackage{tgheros}
\usepackage{subfig}

\renewcommand{\labelitemii}{$\circ$}

\newcommand{\domain}[1]{\emph{#1}}
\newcommand{\ddomain}[1]{\textbf{\domain{#1}}}
\newcommand{\tmdl}[1]{\texttt{#1}}
\newcommand{\tmdlitem}[1]{\texttt{#1}}
\newcommand{\tmdlother}[1]{\emph{#1}}
\newcommand{\tmdllib}{TMDL/Library}
\newcommand{\tmdlscen}{TMDL/Scenario}
\newcommand{\tmdlsort}{\texttt{S}}

\newcommand{\tint}{\mathtt{Int}}
\newcommand{\tintset}{\mathtt{Set\{Int\}}}
\newcommand{\tbool}{\mathtt{Bool}}
\newcommand{\treal}{\mathtt{Real}}
\newcommand{\trealset}{\mathtt{Set\{Real\}}}
\newcommand{\tterms}[2][\Delta]{\mathtt{TERM}(O\cup{#1})_{#2}}

\newcommand{\ant}{\mbox{AN-T}}
\newcommand{\sant}{\mbox{SAN-T}}

\def\sectionautorefname{Section}
\def\subsectionautorefname{Section}
\def\subsubsectionautorefname{Section}

\hypersetup{
    colorlinks = true,
    citecolor = blue,
    linkcolor = red!70!black
}

\begin{document}

\title{Stochastic Activity Networks Templates}
\author{
  Leonardo Montecchi\\
  \texttt{leonardo@ic.unicamp.br}
  \and
  Paolo Lollini\\
  \texttt{lollini@unifi.it}
  \and
  Andrea Bondavalli\\
  \texttt{bondavalli@unifi.it}  
}
\date{Resilient Computing Lab\\University of Firenze\\Technical Report RCL180401\\v1.0\\(29 April 2018)}
\maketitle
 
\begin{abstract}
This technical report defines 
Stochastic Activity Networks Templates (SAN-T), a formalism based on Stochastic
Activity Networks (SAN), with the addition of variability aspects. Its purpose
is to define library of model templates, as in \cite{EDCC14}.
% 
% In this section we introduce the concept of a template-level formalism based on 
% the Stochastic Activity Networks (SANs) formalism.
In \autoref{sec:san} we recall the definition of Stochastic Activity Networks
(SAN), while in \autoref{sec:sant} we introduce SAN
Templates (\sant). The \emph{concretize()} function to generate a SAN
concrete model from a \sant{} is defined in \autoref{sec:sant-instances}.
Finally, the notion of compatibility between a template specification, and its
implementation with SAN-T is defined in \autoref{sec:sant-compatibility}.
% The definitions in the following use the notation introduced in
% \autoref{sec:def-basic}.
\end{abstract}

% \section{SAN-Based Templates Implementations}
% \label{sec:if-using-san}

% Atomic model templates consist of a \emph{specification} and an
% \emph{implementation}. The specification is described using TMDL, the
% implementation using a state-based stochastic formalism of choice.
% Actually, a template implementation uses a modified version of some
% state-based formalism, to accommodate for the variability and parameters.


% The concept of instance of a \sant{} is given in
% In \autoref{sec:sant-instances}.

\section{Stochastic Activity Networks}
\label{sec:san}

A formal definition of Stochastic Activity Networks (SANs) was given by Sanders
and Meyer in \cite{Sanders02}. We recall here the basic definitions provided in
that paper.

An Activity Network (AN) is defined as an eight-tuple \cite{Sanders02}
\begin{equation}
AN = (P, A, I, O, \gamma, \tau, \iota, o),
\end{equation}

where:
  $P$ is a finite set of places;
  $A$ is a finite set of activities;
  $I$ is a finite set of input gates;
  $O$ is a finite set of output gates;
  $\gamma\colon A\to\mathbb{N}^+$ specifies
  the number of cases for each activity;
  $\tau\colon A \to \{\mathrm{timed},\mathrm{instantaneous}\}$
  specifies the type of each activity;
  $\iota\colon I\to A$ maps input gates to activities;
  $o\colon O\to\{(a,c)\mid a\in A\textrm{ and }c
  \in\{1,2,\ldots,\gamma(a)\}\}$ maps output gates to cases of activities.
% \end{defi}

We also recall that, if $S$ is a set of places ($S\subseteq P$), a
\emph{marking} of $S$ is a mapping $\mu\colon S\to\mathbb{N}$.
The value $\mu(p)$ is the
marking of place $p$, i.e., the number of tokens it holds.
Similarly, the set of possible markings of
$S$ is the set of functions $M_S=\{\mu\mid\mu\colon S\to\mathbb{N}\}$.

An input gate is defined to be a triple,
$(G,e,f)$, where $G\subseteq P$ is the set of input places associated with the gate, $e\colon
M_G\to\{0,1\}$ is the enabling predicate of the gate, and $f\colon M_G\to M_G$
is the input function of the gate.
An output gate is a pair, $(G,f)$, where $G\subseteq P$ is the set
of output places associated with the gate, and $f\colon M_G\to M_G$
is the output function of the gate.

Given an activity network that is
stabilizing in some specified initial marking $\mu_0\in M_P$, a Stochastic
Activity Network (SAN) is formed by adjoining functions assignments $C$, $F$,
and $G$, where for each activity $a$, $C_a\in C$ is a function specifying the
probability distribution of its cases; $F_a\in F$ is a function specifying the
probability distribution of its firing delay time; and $G_a\in G$ is a function
that describes its reactivation markings \cite{Sanders02}.

Formally:
\begin{equation}
\begin{split}
SAN &= (AN,\mu_0,C,F,G)=\\
&=\left((P, A, I, O, \gamma, \tau, \iota, o),\mu_0,C,F,G\right).
\end{split}
\end{equation}

\section{SAN Templates}
\label{sec:sant}

We base on the previous definition to define Stochastic Activity Networks
\emph{Templates} (\sant{}), i.e., SANs in which some elements are not fully
defined, but instead they depend on some parameters. 

The purpose of defining SAN-Ts is to be able to specify parametric models based
on the SAN formalism, to be employed as template-level formalism to define
implementations of atomic templates.
From a \sant{} model
different variants of a base SAN skeleton can be generated, based on the values
assigned to the parameters in $\Delta$.
Place templates are mapped to a finite number of ``normal'' places, based on
their multiplicity functions; similarly, activity templates are mapped
to ordinary activities with a finite number of cases.

% This notion of template AN models will
% constitute a fundamental part of the definition of our ``template'' models
% approach.

% \begin{defi}[Parametric Activity Networks]
A \emph{Stochastic Activity Network Template} (\sant) is a tuple
\begin{equation}
{SAN\text{-}T} = (\Delta,\tilde{P},\tilde{A},\tilde{I},\tilde{O},
\tilde{\gamma}, \tilde{\tau}, \tilde{\iota},\tilde{o}, \tilde\mu_0,\tilde
C,\tilde F,\tilde G),
\end{equation}
 where
$\Delta$ is a set of parameters, and elements marked
with a tilde accent, $\tilde{\cdot}$, are modified versions of elements
existing in plain SANs, modified to take parameters into account. In more
details:
\begin{itemize}
  \item $\Delta$ is a sorted set of parameters of the template.
%   Upon instantiation of the template, values in $\mathbb{R}$ will be assigned
%   to each template parameter.
  \item $\tilde{P}$ is a finite set of place templates. A place template can be
  seen as a placeholder for multiple places that, in a regular SAN model,
  would be strongly related to each other.
  It is the case, for example, of places connected to different cases of the
  same activity. Based on parameters' values, a template place will be
  expanded to a precise set of places.

  Formally, a place template is defined by a pair $(\tau,k)$, where
  $\tau$ is the name of the place, and
  $k\in\tterms{\tintset}$ is its multiplicity.
  Evaluating the term $k$ with respect to an assignment $\xi$ identifies a set
  of integer indices $K\subset\mathbb{N}$.
  Such indices identify the set of places
  that, with the given
  setting of parameters, the template place is expanded to.
  Normal places (i.e., those always expanding to a single place of
  ordinary SANs) are those for which $Val_\xi(k)=\{1_\tint\}$ for any assignment
  of parameters $\xi$.
  \item $\tilde{A}$ is a finite set of activity templates.
  \item $\tilde{I}$ is a finite set of input gate templates.
  \item $\tilde{O}$ is a finite set of output gate templates.
  \item $\tilde{\gamma}\colon
  \tilde{A}\to\tterms{\tint}$ specifies the number of
  cases for each activity template. 
  For each activity template $\tilde{a}\in\tilde{A}$, evaluating
  $\tilde{\gamma}(\tilde{a})$ with respect to an assignment $\xi$ returns an
  integer number.
  If $Val_\xi(\tilde{\gamma}(\tilde{a}))=m_\tint$ for any assignment $\xi$,
  then the activity is a regular activity
  having a fixed number of cases $m$.
  \item $\tilde{\tau}\colon \tilde{A} \to
  \{\mathrm{timed},\mathrm{instantaneous}\}$ specifies the type of each activity
  template.
  \item $\tilde{\iota}\colon \tilde{I}\to \tilde{A}$ maps input
  gate templates to activity templates.
%   \item $o\colon O_\Delta\to\{(a,c)|a\in A\textrm{ and }c
%   \in\mathbb{N}\}$ maps parametric output gates to cases of
%   activities.
  \item $\tilde{o}\colon \tilde{O}\to\tilde{A}$ maps output gate templates to
  activity templates.
\end{itemize}
% \end{defi}

% A parametric place $p_\delta\in P_\Delta$ is a pair $(p,\delta)$, with
% $\delta\in\Delta$. Similarly, a parametric activity $a_\delta$ is a pair
% $(a,\delta)$, with $\delta\in\Delta$.

In order to correctly define input and output gate templates, the
concept of marking needs to be extended, making it applicable to place templates.
The idea is to let the marking function anticipate that the place template will
be mapped to a set of places, and thus allow the marking for each of them to be
specified, through an index value.
%
Formally, if $\tilde{S}\subseteq\tilde{P}$ is a set
of place templates, a \emph{marking} of $\tilde{S}$ is a mapping
$\tilde\mu\colon \tilde{S}\times\mathbb{N}\to\mathbb{N}$.
For example, $\tilde\mu(\tilde p,2)=10$, with $\tilde p\in\tilde S$, means
that the place generated from $\tilde p$ having index 2 contains 10 tokens.
Similarly, the set of possible markings of $\tilde S$ is the set of functions
$\tilde M_{\tilde S}=\{\tilde\mu\mid\tilde\mu\colon
\tilde S\times\mathbb{N}\to\mathbb{N}\}$.
% A constraint needs to be imposed for regular places (i.e.,
% non-parametric ones), so that for them the value of the marking function is
% independent on the index. This is expressed in the following:
% \[
% \tilde\mu(p,i)=\tilde\mu(p,j)\quad\forall
% i,j\in\mathbb{N},\quad\forall p=(n,\emptyset)\in\tilde P.
% \]

% Given $S\subseteq(P\cup P_\Delta)$, this definition can be generalized for both
% normal places and parametric places as:
% \[
% \begin{split}
% &M_{S}=\{\mu|\mu\colon
% S\times\mathbb{N}\to\mathbb{N}\},\\
% & \mu(p,n)=\mu(p,m)\qquad \forall p\in P\quad\forall n,m\in\mathbb{N}.
% \end{split}
% \]

An \emph{input gate template} is also defined as a triple
$(\tilde{G},\tilde{e},\tilde{f})$, where $\tilde G\subseteq \tilde{P}$ is the
set of input places associated with the gate, $\tilde{e}\colon
\tilde M_{\tilde G}\to\tterms{\tbool}$ is the enabling
predicate of the gate, and $\tilde f\colon \tilde M_{\tilde
G}\times\Xi\to \tilde M_{\tilde G}$ is the input function of the gate, where
$\Xi$ is the set of all possible assignments.
An input gate template will always result in a single input gate in the concrete
SAN model.
The enabling predicate and the input function of the gate are instead
parametric, i.e., in they depend on all parameters of the template.
% Similarly, a \emph{parametric output gate} is a pair
% $(G,f)$, where $G\subseteq P$ is the set of output places associated with the gate and $f\colon
% M_G\times\mathbb{N}^{|\Delta|}\to M_G$ is the output function of
% the gate.
% It should be noted that the input and output functions of gates are extended
% to take into account for all model parameters, which can then be used to define
% gate functions.

An \emph{output gate template} is a
pair $(\tilde G,\tilde f)$, where $\tilde G\subseteq\tilde P$ is the set of
output places associated with the gate, and
$\tilde f \colon \tilde M_{\tilde
G}\times\mathbb{N}\times\Xi\to \tilde M_{\tilde G}$
is the output function of the gate.
It should be noted that the output function $\tilde f$ depends on the index of
the case of the associated activity template ($\mathbb{N}$), as well as from the
assignment of values to parameters of the \sant{} model ($\Xi$).
In fact, an \emph{output gate template} will be expanded to multiple concrete
output gates, depending on the number of cases of the activity to which it is
connected.

As in \cite{Sanders02}, the input places of an activity template $a$ consist of
the set
$\tilde{IP}(a)=\{p\mid\exists(\tilde{G},\tilde{e},\tilde{f})\in\tilde\iota^{-1}(a)\text{
such that } p\in \tilde G\}$.
Similarly, the output places of an activity template $a$ consist of the set
$\tilde{OP}(a)=\{p\mid\exists(\tilde{G},\tilde{f})\in\tilde{o}^{-1}(a)\text{
such that } p\in\tilde{G}\}$.
% 7. the output places of an activity a consist of the set OP (a) = {p | for some
% c = 1, 2, . . . , γ(a), ∃ (G, f ) ∈ o −1 (a, c) such that p ∈ G}.

Finally:
\begin{itemize}
  \item $\tilde\mu_0\colon\Xi\rightarrow\tilde M_{\tilde P}$ is
  the initial marking function, which defines the initial marking based on
  parameters' assignment.
  \item $\tilde C$ is the case distribution assignment, an assignment of
  functions to activities templates such that for any activity template
  $\tilde a\in\tilde A$, function $\tilde C_{\tilde a}$ defines the probability
  distribution of activity cases. Note that this also depends on parameters;
  thus $\tilde C_{\tilde a}\colon \tilde M_{\tilde{IP}(\tilde a)\cup
  \tilde{OP}(\tilde a)}\times \mathbb{N}^+\times\Xi \to[0,1]$.
  For the model to be well-formed,
  $\tilde C_{\tilde a}(\mu,i,\xi)=0$ should hold
  $\forall i>Val_\xi(\tilde\gamma(\tilde a))$, i.e., the
  probability of cases that are not foreseen by the given parameters
  assignment $\xi$ should be zero.
  \item $\tilde F$ is the activity time distribution function assignment,
  an assignment of continuous functions to timed template activities such that
  for any timed activity template $a$, function $\tilde F_a\colon \tilde
  M_{\tilde P}\times \mathbb{R}\times\Xi\to[0,1]$ defines
  its firing time distribution.
  \item $\tilde G$ is the reactivation function assignment, an assignment
  of functions to timed activities such that for any timed activity $a$,
  function $\tilde G_a\colon \tilde M_{\tilde P}\to \wp(\tilde M_{\tilde P})$,
  defines the reactivation markings, where
  $\wp(\tilde M_{\tilde P})$ denotes the power set of $\tilde M_{\tilde P}$.
\end{itemize}

Note that the definition of SANs \cite{Sanders02} requires that the initial
marking $\mu_0(\xi)\in\tilde M_{\tilde P}$ is a stable
marking in which the network is stabilizing.
However, since the actual structure of a SAN is not completely
specified until a value is assigned to all the \sant{} parameters, we
relax this constraint here.
When parameter values are assigned to a SAN-T, well-formedness
checks on the structure of the resulting model could be performed based on
techniques available for ordinary SAN models (e.g., \cite{Deavours99}).

% \begin{figure}
% \centering
% \beginpgfgraphicnamed{img/san-template-detailed}
% \begin{tikzpicture}
% 	\node (san)
% 	{\includegraphics[scale=.6]{img/hidenets-template-user}};
% 	\node (params) at (6.6,1.7) {
% 		\large
% 		$\Delta=\{n,s_1,s_2,\ldots,p_1,p_2,\ldots\}$
% 	};
% 	\node (tplaces) at (6.2,0.4) {
% 		\footnotesize
% 		$\begin{aligned}
% 		k_{Idle}(\cdot) &= \{1\},\\
% 		k_{Req}(\cdot) &= \{s_1,\ldots, s_n\},\\
% 		k_{Failed}(\cdot) &= \{1\},\\
% 		k_{Dropped}(\cdot) &= \{1\},
% 		%a,b\in&\mathbb{R}
% 		\end{aligned}$
% 	};
% 	\node (tactivities) at (6.3,-1) {
% 		\footnotesize
% 		$\begin{aligned}
% 		\tilde\gamma(\mathtt{Request},\cdot)&=n,\quad
% 		\tilde\gamma(\mathtt{Fail},\cdot)=1,\quad
% 		\tilde\gamma(\mathtt{Drop},\cdot)=1,\\
% 		\tilde{C}_{\mathtt{Request}}(\tilde\mu,i,\cdot)&=p_i
% 		\end{aligned}$
% 	};
% 	\node at (-1,2) {\footnotesize\textsf{
% 		\emph{User} SAN template}
% 	};
% 	\node at (1.1,-2.3) {\footnotesize\textsf{\emph{IUser} model interface}};
% \end{tikzpicture}
% \endpgfgraphicnamed
% 	\caption{Example of a SAN template that encodes the \emph{UserX}
% 	SAN model of \protect\autoref{fig:motivating}. Variable elements are surrounded
% 	by a dashed line. Places surrounded by a yellow filled box are
% 	interface variables of the template. Collectively, they constitute the
% 	\emph{IUser} model interface.}
% 	\label{fig:san-template-example}
% \end{figure}
% An example of a SAN template is shown in \autoref{fig:san-template-example};
% such template could be used to generate the \emph{UserX} model of
% \autoref{fig:motivating}.
%
% Since we want to specify precise service identifiers, this template model has a
% set parameter $n$, which specifies the number of used services, and two
% (potentially infinite) set of parameters: $s_1,s_2,\ldots$, which specify the
% identifiers of such services, and $p_1,p_2,\ldots$, which specify their
% selection probability.
% Note that in general having even a single parameter or a finite set of
% parameters is possible, and depends on individual templates.

% As specified by function $\hat{\gamma}$, the number of cases of activity
% \texttt{Request} is given by parameter $n$, while the activities \texttt{Fail}
% and \texttt{Drop} have always one single case. The function
% $\tilde{C}_{\mathtt{Request}}\in\tilde{C}$ specifies the case selection
% probabilities for activity \emph{Request}. In this case, for each marking
% $\tilde\mu$, the selection probability of case $i$ is given by the value
% of parameter $p_i$.

% The $k_X$ functions specify that
% places \texttt{Idle}, \texttt{Failed}, and
% 	\texttt{Dropped} are single places, while the multiplicity of \texttt{Req}
% depends on the values assigned to parameters.
% In particular, the multiplicity of \texttt{Req} is given by the values assigned
% to the first $n$ parameters among the $s_1,s_2,\ldots$ list.
% 
% Place templates \texttt{Req}, \texttt{Failed}, and \texttt{Dropped} are
% interface variables of the template; collectively, they constitute the
% \emph{IUser} model interface.
% %
% Other elements fully defining the
% template (e.g., the definition of template output gate \texttt{OGRequest}) are
% omitted for simplicity.

\section{Mapping \sant{} to SAN}
\label{sec:sant-instances}

In this section we define the \emph{concretize()} function that generates an
ordinary SAN model from a from a pair $(\text{\sant},\xi)$, that is, from a
\sant{} model and an assignment of values to its parameters.
We first need to introduce some preliminary definitions.

\subsection{Preliminary Definitions}

Given a place template of a \sant{} model, $\tilde p=(\tau,k)\in
\tilde{P}$, and an assignment of parameters $\xi$, we denote with
${\Pi}(\tilde p,i)=p_i\in P^\xi$ the $i$-th place originating from
the template place $\tilde p$ in the \sant{} instance.
% \[
%   \Pi(\tilde{p},i)=\left\{
%   \begin{array}{ll}
%   p, \forall i\in\mathbb{N} & \text{if } \tilde{p}=(p,\emptyset)\in \tilde{P},\\
%   p_i\in\{p_1,\ldots,p_{\xi(\delta)}\} & \text{if } \tilde{p}=(p,\delta)\in
%   \tilde{P}.
%   \end{array}
%   \right.
% \]
Given a marking of a \sant, $\tilde{\mu}\in\tilde M_{\tilde{P}}$, and an
assignment function $\xi$, we denote the corresponding
marking of the \sant{} instance, $\mu\in M_P$, with
${\Gamma}(\tilde{\mu})$. It is obtained as:
\begin{equation}
\mu(\Pi(\tilde p,i))=\tilde{\mu}(\tilde p,i),\qquad\forall \tilde
p\in\tilde P.
\end{equation}
Consequently, given a marking $\mu'$ of the SAN instance we denote as
$\Gamma^{-1}(\mu')$ the corresponding marking of the \sant.

Given an input gate template $\tilde{g}=(\tilde{G},\tilde{e},\tilde{f})\in
\tilde{I}$, and an assignment $\xi$, we denote with
${\alpha}(\tilde{g})$ the corresponding input gate $g=(G,e,f)\in I$ in the \sant{} instance. It is obtained as:
\begin{equation}
\begin{split}
G &= \left\{ p_i=\Pi(\tilde p,j)\mid j\in Val_\xi(k),\; \tilde p=(\tau,k)\in
\tilde{G} \right\},
\\
e(\Gamma(\mu))&=Val_\xi(\tilde
e\left(\tilde{\mu})\right),\\
f(\Gamma(\mu))&=\tilde
f\left(\tilde{\mu},\xi\right).
\end{split}
\end{equation}

% Similarly, given an output gate template $\tilde{g}=(\tilde{G},\tilde{f})$ we
% denote with $\mathbf{\beta}(\tilde{g})$ the corresponding output gate
% $g=(G,f)$ in the SAN instance.
% It is obtained as:
% \begin{align*}
% G' &= (G\cap P)\cup\left(\bigcup_{(p,\delta)\in G\cap
% P_\Delta}\left\{p_1,\ldots,p_{\xi(\delta)}\right\}\right),\\
% f'(\mu')&=f\left(\Gamma^{-1}(\mu'),\xi(\delta_1),\ldots,\xi(\delta_{|\Delta|})\right).
% \end{align*}

Finally, given an output gate template
$\tilde g=(\tilde{G},\tilde{f})\in \tilde O$, and an assignment function $\xi$,
we denote with ${\beta}(\tilde g,i)$ the $i$-th output gate $g_i=(G_i,f_i)\in O$
generated from it in the SAN model. It is obtained as:
\begin{equation}
\begin{split}
G_i &= \left\{ p_i=\Pi(\tilde p,j)\mid j\in Val_\xi(k),\; \tilde p=(\tau,k)\in
\tilde{G} \right\},
\\
f_i(\Gamma(\mu))&=\tilde
f\left(\tilde\mu,i,\xi\right).
\end{split}
\end{equation}


\subsection{concretize()}


Given a SAN-T $S_\Delta$:
\begin{equation}
S_\Delta = (\Delta,\tilde{P},\tilde{A},\tilde{I},\tilde{O},
\tilde{\gamma},\tilde{\tau},\tilde{\iota},\tilde{o},\tilde{\mu}_0,\tilde{C},\tilde{F},\tilde{G}),
\end{equation}
and a parameter assignment function $\xi$, the corresponding SAN model $S^\xi$
is obtained as follows:
\begin{equation}
S^\xi = (P^\xi, A^\xi, I^\xi, O^\xi, \gamma^\xi, \tau^\xi, \iota^\xi,
o^\xi,\mu_0^\xi,C^\xi,F^\xi,G^\xi),
\end{equation}
where:
% \footnotemark:
% \footnotetext{For the sake of readability, in the following we use some notation
% whose definition is given only intuitively. Informally, we denote with
% $\Gamma(.)$ a function that maps markings of the SAN-T to markings of the SAN
% instance. Similarly, $\alpha(.)$, $\beta(.,i)$, denote functions that map input
% gate and output gate templates to regular input and output gates, respectively.
% Formal definitions of those concepts are given in \autoref{sec:mapping-details}.}

\begin{equation}
\begin{split}
P^\xi&=
	\bigcup_{(\tau,k)\in\tilde{P}}{
		\left\{\tau_i\mid i\in
		Val_\xi(k)\right\} };
\\
A^\xi&=\tilde{A};
\\
\gamma^\xi(a)&=Val_\xi(\tilde\gamma\left(\tilde a\right));
\\
I^\xi&=\left\{\alpha(\tilde g)\mid\tilde g\in \tilde{I}\right\};
\\
O^\xi&=\textstyle{\bigcup_{\tilde
g\in\tilde{O}}\left\{\beta(\tilde g,1),\ldots,\beta(\tilde
g,Val_\xi(\tilde\gamma\left(\tilde a\right)))\mid \tilde a=\tilde{o}(\tilde g)\right\}};
\\
\tau^\xi&=\tilde{\tau};
\\
\iota^\xi(\alpha(g))&=\tilde{\iota}(g),\quad\forall g\in\tilde{I};
\\
o^\xi(\beta(g,i))&=\tilde{o}(g),\quad\forall g\in \tilde{O},\forall
i\in
\left\{1,\ldots,Val_\xi(\tilde\gamma\left(\tilde a\right))\right\};
\\
\mu_0^\xi&=\tilde{\mu}_0(\xi).
\end{split}
\end{equation}
Furthermore:
\begin{itemize}
  \item $C^\xi$: for each function $\tilde C_{\tilde a}\in\tilde C$ in the case
  distribution assignment $\tilde{C}$ a corresponding function $C_a^\xi$ is included in $C^\xi$,
  defined as
  $C_a^\xi\left(\Gamma{(\mu)},k\right)=\tilde
  C_{\tilde a}\left(\mu,k,\xi\right),
  \;\forall\mu\in M_{\tilde{P}},\;\forall k\in\mathbb{N^+}$.
  \item $F^\xi$: for each function $\tilde F_{\tilde a}$ in the activity time
  distribution assignment $\tilde{F}$ a corresponding function $F_a^\xi$ is
  included in $F^\xi$, defined as
  $F_a^\xi(\Gamma{(\mu)},r)=\tilde F_{\tilde a}(\mu,r,\xi), \;\forall\mu\in
  M_{\tilde{P}},\;\forall r\in\mathbb{R}$.
  \item $G^\xi$: for each function $\tilde G_{\tilde a}\in\tilde{G}$ in the
  reactivation function assignment $\tilde{G}$ a corresponding function $G_a^\xi$ is added to
  $G^\xi$, defined as $G_a^\xi(\Gamma{(\mu)})=\big\{\Gamma(\tilde\mu)\mid \tilde\mu\in\tilde
  G_a(\tilde\mu)\big\}\; \forall\mu\in\tilde M_{\tilde S}$.
\end{itemize}



% \begin{figure}
% \centering
% \beginpgfgraphicnamed{img/template-and-instances}
% \begin{tikzpicture}
% 	\node[draw,thick] (template) at (0,2)
% 	{
% % 		\includegraphics[scale=.7]{img/san-template-detailed}
% 		\begin{tikzpicture}
% 			\node (san)
% 			{\includegraphics[scale=.5]{img/hidenets-template-user-simple}};
% 			\node (params) at (0,2) {
% 				\large
% 				$\Delta=\{n,s_1,s_2,\ldots\}$
% 			};
% 			\node (tplaces) at (0,-2.4) {
% 				\footnotesize
% 				$\begin{aligned}
% 				k_{Idle}(\cdot) &= \{1\} \\
% 				k_{Req}(\cdot) &= \{s_1,\ldots,s_n\} \\
% 				k_{Failed}(\cdot) &= \{1\} \\
% 				k_{Dropped}(\cdot) &= \{1\}\\
% 				%a,b\in&\mathbb{R}
% 				\end{aligned}$
% 			};
% 			\node (tactivities) at (0,-4) {
% 				\footnotesize
% 				$\begin{aligned}
% 				\tilde\gamma(\mathtt{Request},\cdot)&=n\\
% 				\tilde\gamma(\mathtt{Fail},\cdot)&=1\\
% 				\tilde\gamma(\mathtt{Drop},\cdot)&=1
% 				\end{aligned}$
% 			};
% 		\end{tikzpicture}
% 	};
% 	\node (variant1) at (8,4.2)
% 	{\includegraphics[scale=.45]{img/hidenets-user-variant1}};
% 	\node (variant2) at (8,0)
% 	{\includegraphics[scale=.45]{img/hidenets-user-variant2}};
%
% 	\node[above of=template,node distance=3.8cm] {
% 		\footnotesize{SAN template \emph{User}}
% 	};
% 	\node[above of=variant1,node distance=1.8cm] {
% 		\footnotesize{$\text{\emph{User}}(3,1,6,7,\ldots,0.7,0.2,0.1,\ldots)$}
% 	};
% 	\node[above of=variant2,node distance=2cm] {
% 		\footnotesize{$\text{\emph{User}}(4,2,3,5,6,\ldots,0.05,0.15,0.5,0.3,\ldots)$}
% 	};
%
% 	\draw[-triangle 60,thick] (template.20) -- (variant1.west)
% 		node[pos=0.55,sloped,above] {\footnotesize $\xi(n)=3$ }
% 		node[pos=0.55,sloped,below] {\footnotesize $\xi(s_1)=1,\;\xi(s_2)=6,\;\ldots$
% 		}; \draw[-triangle 60,thick] (template.-20) -- (variant2.west)
% 		node[pos=0.55,sloped,above] {\footnotesize $\xi(n)=4$ }
% 		node[pos=0.55,sloped,below] {\footnotesize $\xi(s_1)=2,\;\xi(s_2)=3,\;\ldots$
% 		};
% \end{tikzpicture}
% \endpgfgraphicnamed
% \caption{Example of two different \sant{} instances created from the same
% template, using different assignments for its parameters.}
% \label{fig:sant-instances}
% \end{figure}

% % \section{Additional Formal Definitions}
% % \label{sec:otherdefs}
% 
% This appendix contains formal definitions for notation used in
% \autoref{sec:sant-instances}, but whose precise definition was omitted from
% there for readability purposes.

\section{Compatibility Notion for \sant{} Models}
\label{sec:sant-compatibility}

In this section we describe the constraints that need to be enforced when
applying the TMDL framework using SAN-T as the template-level formalism. In
particular, we define the notion of compatibility between an atomic model template specification, and
the corresponding implementation with \sant{}.

Given an \emph{atomic template specification}: 
\begin{equation}
MT=(\mathcal{I},\Delta,O,L_T,\Psi=\mathcal{M}),
\end{equation}
and its \emph{implementation} as a \sant{} model:
\begin{equation}
\mathcal{M}=
(\Delta_{\mathcal{M}},\tilde{P},\tilde{A},\tilde{I},\tilde{O},
\tilde{\gamma}, \tilde{\tau}, \tilde{\iota},\tilde{o}, \tilde\mu_0,\tilde
C,\tilde F,\tilde G),
\end{equation}
the implementation $\mathcal{M}$ is \emph{compatible} with the
specification $(\mathcal{I},\Delta,O,L_T)$ if and only if the following
conditions hold:
\begin{enumerate}
  \item\label{item:compat1} $\Delta=\Delta_{\mathcal{M}}$.
  \item\label{item:compat2} 
  $\forall (v,\Delta^v,L,k)\in\mathcal{I}$,
  $\exists (\tau,k_\tau)\in\tilde P$ such that $Val_\xi(k)=Val_\xi(k_\tau)$
  for any assignment $\xi$.
  \item\label{item:compat3}
  $\forall (v,\Delta^v,L,k)\in O$,
  $\exists (\tau,k_\tau)\in\tilde P$ such that $Val_\xi(k)=Val_\xi(k_\tau)$
  for any assignment $\xi$.
  \end{enumerate}
Condition~\ref{item:compat1} states that the specification and the
implementation must have the same set of parameters.
Condition~\ref{item:compat2} states that for each meta-variable in the
interfaces realized by the template specification, a template place having 
the same multiplicity for must exist in the \sant{} implementation.
Precisely, the multiplicities of the interface variable and of the
template place must be the same for any possible assignment of values to model parameters.
Condition~\ref{item:compat3} states the same for observation points.

The original SAN definitions in \cite{Sanders02}, as well as SAN-T defined in
\autoref{sec:sant} only consider integer-valued places. In case \emph{extended
places} of the M\"obius implementation are used \cite{Courtney09}, conditions
\ref{item:compat2} e \ref{item:compat3} must be modified to state that
the \emph{sort} the meta-variable and the template places must be
of the same \emph{sort}:

\begin{enumerate}
  \item[2a.]\label{item:compat2a} 
  $\forall (v_s,\Delta^v,L,k)\in\mathcal{I}$,
  $\exists (\tau_{s'},k_\tau)\in\tilde P$ such that $s=s'$ and
  $Val_\xi(k)=Val_\xi(k_\tau)$ for any assignment $\xi$.
  \item[3a.]\label{item:compat3a}
  $\forall (v_s,\Delta^v,L,k)\in O$,
  $\exists (\tau_{s'},k_\tau)\in\tilde P$ such that $s=s'$ and
  $Val_\xi(k)=Val_\xi(k_\tau)$ for any assignment $\xi$.
  \end{enumerate}

\bibliographystyle{IEEEtran}
\bibliography{templates-paper}

\end{document}
